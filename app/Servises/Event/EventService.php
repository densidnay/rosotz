<?php

namespace App\Servises\Event;

use App\Http\Controllers\Controller;
use App\Http\Requests\EventCreateRequest;
use App\Models\Event;
use App\Servises\Queue\QueueController;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class EventService extends Controller
{
    const CACHE_TIME = 5;
    const EVENT_CREATE_LOG_NAME = 'EventCreate';

    private QueueController $queueService;

    public function __construct(QueueController $queueService)
    {
        $this->queueService = $queueService;
    }

    public function createEvent(EventCreateRequest $request)
    {
        $event = $request->validated();

        $eventDate = Carbon::parse($event['date']);
        $diffDate = $this->getDiffDate($eventDate);
        try {
            $event = Event::create(array_merge($event, $diffDate));

            Cache::put("event_{$event->id}", $event, now()->addMinutes(self::CACHE_TIME));
            Cache::delete('eventList');

            $res = $this->queueService->publish($event);
        } catch (\Throwable $e) {

            Log::channel(self::EVENT_CREATE_LOG_NAME)->debug("Create event error: " . $e->getMessage());
            return $e;
        }

        return $event;
    }

    private function getDiffDate($eventDate): array
    {
        $now = Carbon::now();
        $interval = $now->diffAsCarbonInterval($eventDate, false);
        $periodType = 'день';
        $period = 1;

        if ($interval->y) {
            $periodType = 'год';
            $period = $interval->y;
        } else if ($interval->m) {
            $periodType = 'месяц';
            $period = $interval->m;
        } else if ($interval->d > 1) {
            $period = $interval->d;
        }
        $period = ($interval->invert) ? "-{$period}" : $period;

        return ['period_type' => $periodType, 'period' => $period];
    }
}
