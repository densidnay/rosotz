<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'place', 'date', 'period', 'period_type'];

    public function formatDate(): string
    {
        return Carbon::parse($this->date)->format('d.m.Y');
    }

    public function setPeriod(): string
    {
        if ($this->period < 0) {
            $period = abs($this->period);
            return "было {$period} {$this->period_type} назад";
        } else {
            return "через {$this->period} {$this->period_type}";
        }
    }
}
