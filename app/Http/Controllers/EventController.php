<?php

namespace App\Http\Controllers;

use App\Http\Requests\EventCreateRequest;
use App\Models\Event;
use App\Servises\Event\EventService;
use Illuminate\Support\Facades\Cache;

class EventController extends Controller
{
    const CACHE_TIME = 5;

    public function index()
    {
        if (Cache::has('eventList')){
            $events = Cache::get('eventList');
        } else {
            $events = Event::all();
            Cache::put('eventList', $events, now()->addMinutes(self::CACHE_TIME));
        }

        return view('event.index', compact('events'));
    }

    public function store(EventCreateRequest $request, EventService $eventService)
    {
        $res = $eventService->createEvent($request);

        if ($res instanceof Event) {

            return redirect()->route('event.index')->with('success', 'Событие добавлено!');
        }

        return redirect()->route('event.index')->with('error', 'Что то пошло не так, мы уже ремонтируем!');
    }
}
