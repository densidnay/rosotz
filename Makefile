stop-all:
	docker stop $(docker ps -a -q)

setup:
	composer install
	cp -n .env.example .env || true
	php artisan key:gen --ansi


start:
	./vendor/bin/sail up -d

stop:
	./vendor/bin/sail down

sail-start:
	sail up -d

sail-stop:
	sail down

stop-mysql:
	sudo service mysql stop

stop-apache:
	sudo service apache2 stop

alias-sail:
	alias sail='[ -f sail ] && sh sail || sh vendor/bin/sail'

migrate:
	./vendor/bin/sail artisan migrate:fresh

front-dev_start:
	sail npm install && npm run dev
