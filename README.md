## if first launch
### Setup 

```sh
$ make setup
```

#### then setup migrate

```sh
$ make migrate
```

####  then start docker

```sh
$ make start
```
## else 
####  only start docker

```sh
$ make start
```