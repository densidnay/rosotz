<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/datepicker.min.css" integrity="sha512-YdYyWQf8AS4WSB0WWdc3FbQ3Ypdm0QCWD2k4hgfqbQbRCJBEgX0iAegkl2S1Evma5ImaVXLBeUkIlP6hQ1eYKQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet"
          href="{{ asset('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

    <title>Event</title>
</head>
<body>
@if (session()->has('success'))
    <div class="alert alert-success" style="z-index: 1000;">
        {{ session('success') }}
    </div>
@endif
@if (session()->has('error'))
    <div class="alert alert-danger" style="z-index: 1000;">
        {{ session('error') }}
    </div>
@endif
<div class="container">

    <form class="col-4 pt-5 pb-5" action="{{ route('event.store') }}" method="post" >
        @csrf
        <div class="form-group">
            <label for="titleInput">Title</label>
            @error('title') <span class="alert-danger">{{ $message }}</span> @enderror
            <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" id="titleInput" value="{{ old('title') }}">
        </div>
        <div class="form-group">
            <label for="placeInput">Place</label>
            @error('place') <span class="alert-danger">{{ $message }}</span> @enderror
            <input type="text" name="place" class="form-control @error('place') is-invalid @enderror" id="placeInput" value="{{ old('place') }}">
        </div>
        <div class="form-group">
            <label>Date</label>
            @error('date') <span class="alert-danger">{{ $message }}</span> @enderror
            <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                <input type="text" name="date"  class="form-control datetimepicker-input @error('date') is-invalid @enderror" data-target="#reservationdatetime"/>
                <div class="input-group-append" data-target="#reservationdatetime" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>


        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">name</th>
                <th scope="col">дата проведения</th>
                <th scope="col">период</th>
            </tr>
            </thead>
            <tbody>
            @forelse($events as $event)
            <tr>
                <th>{{ $event->id }}</th>
                <td>{{ $event->title}} {{ $event->place }}</td>
                <td>{{ $event->formatDate() }}</td>
                <td>{{ ($event->setPeriod()) }}</td>
            </tr>
            @empty
                <tr>
                    <td colspan="4">no events</td>
                </tr>
            @endforelse
            </tbody>
        </table>


</div>
<script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
<script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/plugins/inputmask/jquery.inputmask.min.js') }}"></script>
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>

<script>
    $('#reservationdatetime').datetimepicker(
        {
            format: 'YYYY-MM-DD HH:mm',
            pick12HourFormat: false,
            icons: {time: 'far fa-clock'}
        });
</script>
<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>


</body>
</html>
